describe('Devices tests', () => {
    it('should be 720p', () => {
        cy.viewport(1280, 720);
        cy.visit('https://www.saucedemo.com/');
        cy.wait(3000);
    });

    it('should be 1080p', () => {
        cy.viewport(1980, 1080);
        cy.visit('https://www.saucedemo.com/');
        cy.wait(3000);
    });

    it('should be iPhone X', () => {
        cy.viewport('iphone-x');
        cy.visit('https://www.saucedemo.com/');
        cy.wait(3000);
    });

    it('should be iPad Mini', () => {
        cy.viewport('ipad-mini');
        cy.visit('https://www.saucedemo.com/');
        cy.wait(3000);
    });
});
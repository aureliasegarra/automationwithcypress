describe("Assert Content Type", () => {
  it("should verify browser document content", () => {
    cy.visit("https://example.com");
    cy.wait(2000);
    cy.document().its("contentType").should("eq", "text/html");
    cy.document().should("have.property", "charset").and("eq", "UTF-8");
  });
});

/// <reference types="Cypress" />

describe("Browser actions", () => {
  beforeEach(() => {
    cy.visit("https://books.toscrape.com");
    cy.url().should("include", "books.toscrape.com");
  });

  it("should display the poetry books Olio and its price", () => {
    cy.get("a").contains("Poetry").click();
    cy.get("h1").should("contain", "Poetry");
    cy.get("a").contains("Olio").click();
  });

  it('Should change the current date', () => {
    const date = new Date(2022, 5, 10).getTime();
    cy.clock(date);
    cy.log(date);
  });


});

describe('Login with fixtures data', () => {
    it('should try to login', () => {
        cy.visit('http://zero.webappsecurity.com/');

        cy.fixture('user').then(user => {
            const username = user.username;
            const password = user.password;
            cy.get('#signin_button').click().wait(3000);
            cy.get('#user_login').type(username);
            cy.get('#user_password').type(password);
            cy.contains('Sign in').click();
        });
    });
});
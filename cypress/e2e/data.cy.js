describe("Write or Read data to JSON / Text file", () => {
  it("should write data into JSON", () => {
    cy.writeFile("log.json", { name: "aurelia", age: 43 });
  });

  it("should write data into Text file", () => {
    cy.writeFile("log.txt", "Hello world");
  });

  it("should read data into JSON", () => {
    cy.readFile("log.json")
        .its("name")
        .should("eq", "aurelia");
  });

  it("should read data into Text file", () => {
    cy.readFile("log.txt").should("eq", "Hello world");
  }); 
});
